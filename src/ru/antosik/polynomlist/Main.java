package ru.antosik.polynomlist;

import ru.antosik.polynomlist.Classes.Polynom;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Polynom poly1 = new Polynom(List.of(1, -1));
        Polynom poly2 = new Polynom(List.of(1, 1));

        Polynom multi = Polynom.multiply(poly1, poly2);

        System.out.println(multi);
    }
}
