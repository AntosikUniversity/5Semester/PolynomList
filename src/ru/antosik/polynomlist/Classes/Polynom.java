package ru.antosik.polynomlist.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Polynom {
    private final List<Integer> ratioList;

    public Polynom(List<Integer> degreeRatioMap) {
        this.ratioList = degreeRatioMap;
    }

    public static Polynom multiply(Polynom first, Polynom second) {
        if (first == null || first.isNull() || second == null || second.isNull()) return new Polynom(new ArrayList<>());

        HashMap<Integer, Integer> result = new HashMap<>();

        for (int i = 0, size1 = first.ratioList.size(); i < size1; i++) {
            for (int j = 0, size2 = second.ratioList.size(); j < size2; j++) {
                int newdegree = i + j;
                int newvalue = first.ratioList.get(i) * second.ratioList.get(j);
                if (result.containsKey(newdegree)) result.put(newdegree, result.get(newdegree) + newvalue);
                else result.put(newdegree, newvalue);
            }
        }

        return new Polynom(new ArrayList<>(result.values()));
    }

    public static Polynom multiply(Polynom first, int num) {
        if (first == null || first.isNull() || num == 0) return new Polynom(new ArrayList<>());

        List<Integer> result = new ArrayList<>(first.ratioList.size());
        for (int koef : first.ratioList) {
            result.add(koef * num);
        }

        return new Polynom(result);
    }

    public List<Integer> getRatioList() {
        return ratioList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0, len = ratioList.size(); i < len; i++) {
            int coef = ratioList.get(i);
            if (coef != 0)
                sb.append(String.format("+%d*x^%d", coef, i));
        }

        return sb.toString();
    }

    public boolean isNull() {
        return ratioList.isEmpty() || ratioList.stream().allMatch(integer -> integer == 0);
    }
}
